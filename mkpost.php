<?php
if(!(isset($_POST["name"]))) { // check to make sure name has been provided
	header('Location: post.php');
	
}

if(!(isset($_POST["message"]))) { // check to make sure message has been provided
	header('Location: post.php');
	
}


$name = $_POST["name"]; // get the name
$msg = $_POST["message"]; // get the message
$time = date("F d Y, H:ia"); // get the current post time
$content = '<h2 class="blog-post-title">'.$name.'</h2> <p class="blog-post-meta">'.$time.' UTC</p>'.$msg; // generate the overall message
$id = 1; // get initial starting post id
$file = 'posts/'.$id.'.txt'; // assign it to file name
while(file_exists($file)) { // get next availible post name
$id++;
$file = 'posts/'.$id.'.txt';

}

$dates = unserialize(file_get_contents('dates.txt')); // unload the post dates file
$dates[] = $time; // add this files time
file_put_contents('dates.txt', serialize($dates)); // update the dates file
file_put_contents($file, $content); // create the post
header('Location: view.php?post=posts/'.$id.'.txt'); // forward the user to the post the just made

?>
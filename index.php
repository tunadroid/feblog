<?php
// The php pre-process
// Fetch the newest posts
$show_amount = 3;
$posts = glob('posts/*.txt');
usort($posts, function($a, $b) {
    return filectime($a) < filectime($b);
});

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="./assets/fav.png">
    <title>Feblog Engine</title>
    <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap/css/blog.css" rel="stylesheet">
	<link href="./bootstrap/css/feblog.css" rel="stylesheet">
	<link href="./bootstrap/css/header.css" rel="stylesheet">
    </head>
    <body>
    <div class="blog-header-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item-header active" href="#"><h3 class="blog-nav-item-header">Home</h3></a>
          <a class="blog-nav-item-header" href="archives.php"><h3 class="blog-nav-item-header">Archives</h3></a>
        </nav>
      </div>
    </div>
    <div class="container">
      <div class="blog-header">
        <h1 class="blog-title">The Feblog Engine</h1>
        <p class="lead blog-description">A beautiful, flexible, powerful engine for blogging.</p>
      </div>
      <div class="row">
        <div class="col-sm-8 blog-main">
		  <?php // echo the last few posts (amount defined in $show_amount)
		    $id = 0;
		    while(!($show_amount == 0)) { // display the posts
				echo "<div class='blog-post'>";
		  		echo file_get_contents($posts[$id], true);
				echo "</div>";
				$show_amount--;
				$id++;
				
		  	}
			
		  ?>
          <ul class="pager">
            <li><a href="archives.php">Older</a></li>
            <li><a href="#">Back to Top</a></li>
          </ul>
        </div>
        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <h4>About</h4>
            <p>Feblog is a flexible, customizeable, and powerful blogging engine. The exterior of a Feblog powered blog may look complex, but internally, Feblog is very light-weight and simple. This causes less stress and less headaches. </p>
          </div>
          <div class="sidebar-module">
            <h4>Archives</h4>
            <ol class="list-unstyled">
              <li><a href="#">January 2014</a></li>
              <li><a href="#">December 2013</a></li>
              <li><a href="#">November 2013</a></li>
            </ol>
          </div>
          <div class="sidebar-module">
            <h4>Elsewhere</h4>
            <ol class="list-unstyled">
              <li><a href="#">BitBucket</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">Google+</a></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="blog-footer">
      <p>Feblog blog system by <a href="https://twitter.com/lightningboy24">@lightningboy24</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="./bootstrap/js/bootstrap.min.js"></script>
    <script src="./bootstrap/js/docs.min.js"></script>
  </body>
</html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="./assets/fav.png">
    <title>Feblog Engine</title>
    <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap/css/blog.css" rel="stylesheet">
	<link href="./bootstrap/css/feblog.css" rel="stylesheet">
	<link href="./bootstrap/css/header.css" rel="stylesheet">
	<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
	<script>
        tinymce.init({selector:'textarea'});
	</script>
    </head>
    <body>
    <div class="blog-header-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item-header active" href="index.php"><h3 class="blog-nav-item-header">Home</h3></a>
          <a class="blog-nav-item-header" href="archives.php"><h3 class="blog-nav-item-header">Archives</h3></a>
        </nav>
      </div>
    </div>
	<br/>
    <div class="container">
	  <div class="jumbotron">
        <div class="container">
          <h1>New Post</h1>
          <hr>	                                                
		  <form name="post" onsubmit="fetchTinyMCE()" method="post" action="mkpost.php"> 
          <input type="text" class="form-control" placeholder="Post name" id="name" name="name"><br>
          <textarea type="text" class="form-control" id="tinymce" name="tinymce" rows="6"></textarea><br>
          <input type="hidden" id="message" name="message"><br>
          <input type="submit" style="width: 25%;" value="Post" class="btn btn-info pull-right" />
	      </form>	
	    </div>								 
	  </div>
    </div>
    <div class="blog-footer">
      <p>Feblog blog system by <a href="https://twitter.com/lightningboy24">@lightningboy24</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </div>
    <script>
    function fetchTinyMCE() {
      // Get the HTML contents of the currently active editor
      console.debug(tinyMCE.activeEditor.getContent());
      document.getElementById('message').value = tinyMCE.activeEditor.getContent();
    
    }
    
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="./bootstrap/js/bootstrap.min.js"></script>
    <script src="./bootstrap/js/docs.min.js"></script>
  </body>
</html>

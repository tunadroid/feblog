<?php
// The php pre-process
// Fetch the post to be displayed

//echo basename(realpath(__DIR__.'/posts/'.$post_to_view.'.txt'));
if(isset($_GET['post'])) { // if a post was mentioned fetch it
  $post_to_view = $_GET['post'];
  if(file_exists(__DIR__.'/posts/'.$post_to_view.'.txt')) {
    $post = file_get_contents(__DIR__.'/posts/'.basename(realpath(__DIR__.'/posts/'.$post_to_view.'.txt')));
  }
  
  else { // the post does not exist! Re-route the user to the error page with the error: invalid post
    header ("Location: error.php?cause=invalid%20post%20requested&id=202");
    //echo "it broked";
    
  }
}

else { // no post was mentioned! Re-route the user to the error page with the error: no post
  header ("Location: error.php?cause=no%20post%20was%20mentioned&id=101");
  
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="./assets/fav.png">
    <title>Feblog Engine</title>
    <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap/css/blog.css" rel="stylesheet">
  <link href="./bootstrap/css/feblog.css" rel="stylesheet">
  <link href="./bootstrap/css/header.css" rel="stylesheet">
    </head>
    <body>
    <div class="blog-header-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item-header active" href="#"><h3 class="blog-nav-item-header">Home</h3></a>
          <a class="blog-nav-item-header" href="archives.php"><h3 class="blog-nav-item-header">Archives</h3></a>
        </nav>
      </div>
    </div>
    <div class="container">
      <div class="blog-header">
        <h1 class="blog-title">The Feblog Engine</h1>
        <p class="lead blog-description">A beautiful, flexible, powerful engine for blogging.</p>
      </div>
      <div class="row">
        <div class="col-sm-8 blog-main">
          <div class="blog-post">
      <?php echo $post ?><!-- echo the contents of the post (which are HTML) -->
          </div>
          <ul class="pager">
            <li><a href="archives.php">Older</a></li>
            <li><a href="#">Back to Top</a></li>
          </ul>
        </div>
        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <h4>About</h4>
            <p>Feblog is a flexible, customizeable, and powerful blogging engine. The exterior of a Feblog powered blog may look complex, but internally, Feblog is very light-weight and simple. This causes less stress and less headaches. </p>
          </div>
          <div class="sidebar-module">
            <h4>Archives</h4>
            <ol class="list-unstyled">
              <li><a href="#">January 2014</a></li>
              <li><a href="#">December 2013</a></li>
              <li><a href="#">November 2013</a></li>
            </ol>
          </div>
          <div class="sidebar-module">
            <h4>Elsewhere</h4>
            <ol class="list-unstyled">
              <li><a href="#">BitBucket</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">Google+</a></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="blog-footer">
      <p>Feblog blog system by <a href="https://twitter.com/lightningboy24">@lightningboy24</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="./bootstrap/js/bootstrap.min.js"></script>
    <script src="./bootstrap/js/docs.min.js"></script>
  </body>
</html>
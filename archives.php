<?php
// The php pre-process
date_default_timezone_set("UTC"); // use the UTC timezone
// Fetch the newest posts
$posts = glob('posts/*.txt');
usort($posts, function($a, $b) {
    return filectime($a) < filectime($b);
	
});

$show_amount = count($posts);
$rawdates = unserialize(file_get_contents('dates.txt'));
$dates = array_reverse($rawdates);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="./assets/fav.png">
    <title>Feblog Engine | Archives </title>
    <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap/css/blog.css" rel="stylesheet">
	<link href="./bootstrap/css/feblog.css" rel="stylesheet">
	<link href="./bootstrap/css/header.css" rel="stylesheet">
    </head>
    <body>
    <div class="blog-header-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item-header" href="index.php"><h3 class="blog-nav-item-header">Home</h3></a>
          <a class="blog-nav-item-header active" href="#"><h3 class="blog-nav-item-header">Archives</h3></a>
        </nav>
      </div>
    </div>
    <div class="container">
      <div class="blog-header">
        <h1 class="blog-title">The Feblog Engine</h1>
        <p class="lead blog-description">A beautiful, flexible, powerful engine for blogging.</p>
      </div>
      <div class="row">
        <div class="col-sm-8 blog-main">
		<hr>
		  <?php // echo all the posts
		    $id = 0;
		    while(!($show_amount == 0)) { // display the posts
                // get the date of this post
                $dateid = $id + 1;
				echo "<h4>Date: <a href='view.php?post=$id'>".$dates[$id]." </a> UTC</h4>";
				echo "<hr>";
				$show_amount--;
				$id++;
				
		  	}
			
		  ?>
        </div>
        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <h4>About</h4>
            <p>Feblog is a flexible, customizeable, and powerful blogging engine. The exterior of a Feblog powered blog may look complex, but internally, Feblog is very light-weight and simple. This causes less stress and less headaches. </p>
          </div>
          <div class="sidebar-module">
            <h4>Archives</h4>
            <ol class="list-unstyled">
              <li><a href="#">January 2014</a></li>
              <li><a href="#">December 2013</a></li>
              <li><a href="#">November 2013</a></li>
            </ol>
          </div>
          <div class="sidebar-module">
            <h4>Elsewhere</h4>
            <ol class="list-unstyled">
              <li><a href="#">BitBucket</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">Google+</a></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="blog-footer">
      <p>Feblog blog system by <a href="https://twitter.com/lightningboy24">@lightningboy24</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="./bootstrap/js/bootstrap.min.js"></script>
    <script src="./bootstrap/js/docs.min.js"></script>
  </body>
</html>
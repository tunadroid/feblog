<?php // fetch error if mentioned
$id = "404";
$error = "I can't find the page you just requested!";
if(isset($_GET['id'])) {
	$id = $_GET['id'];
	
}

if(isset($_GET['cause'])) {
	$error = $_GET['cause'];
	
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="The Feblog Engine error page">
    <meta name="author" content="lightningboy24">
    <link rel="shortcut icon" href="./assets/fav.png">
    <title>Feblog Engine</title>
    <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="./bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
  </head>
  <body>
  	<br/>
    <div class="container">
      <div class="jumbotron center">
        <h1>Error <?php echo $id; ?></h1>
		<br/>
        <p style="color: red;"><?php echo $error; ?></p>
        <p><b>This neat little button will bring you back to the homepage:</b></p>
        <a href="index.php" class="btn btn-large btn-info"><span class="glyphicon glyphicon-home"></span> Head to Home</a>
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="./bootstrap/js/bootstrap.min.js"></script>
    <script src="./bootstrap/js/docs.min.js"></script>
  </body>
</html>